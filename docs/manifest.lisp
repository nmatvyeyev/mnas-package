(:docstring-markup-format
   :scriba
   :systems (:MNAS-PACKAGE)
   :documents ((:title "Mnas-Ffile-Dialog"
	        :authors ("Mykola Matvyeyev")
	        :output-format (:TYPE :MULTI-HTML :TEMPLATE :GAMMA) 
                :sources ("mnas-file-dialog.scr" "mnas-file-dialog-graph.scr")
                )))
